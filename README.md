# 3088 PiHat Project_STLTSE004

A public repository for the PiHat project

The PiHat project is a motor driver with a speed and direction feedback. The project uses the Raspberry microHat to track and manage the direction and speed of a motor using 3 submodules that will be descibed below. 

1. Power supply circuitry for motor and driver IC
2. Status LEDs that connects to the motor IC and power circuitry
3. Motor position feedback sensor signal within 0-3.3V

Components used:
-LEDs
-Op-amps
-Resistors
-Capacitors
-diodes
-Potentiometers
-PSU
-microHat
-Motor
-Motor driver IC

Any help on how to better improve the accuracy of the motor's positioning will be highly appreciated.
